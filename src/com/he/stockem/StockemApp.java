package com.he.stockem;

import java.io.File;
import java.io.IOException;

import android.app.Application;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;

public class StockemApp extends Application {

	/**
	 * Called when the application is starting, before any activity, service, or receiver objects (excluding content providers) have been created.
	 */
	
	public void onCreate() {
		super.onCreate();
		
		Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
	    {
	      @Override
	      public void uncaughtException (Thread thread, Throwable e)
	      {
	        handleUncaughtException (thread, e);
	      }
	    });

		if ( isExternalStorageWritable() ) {

			File appDirectory = new File( Environment.getExternalStorageDirectory() + "/Stockem" );
			File logDirectory = new File( appDirectory + "/log" );
			File logFile = new File( logDirectory, "logcat" + "log" + ".txt" );
			
			// create app folder
			if ( !appDirectory.exists() ) {
				appDirectory.mkdir();
			}

			// create log folder
			if ( !logDirectory.exists() ) {
				logDirectory.mkdir();
			}

			// clear the previous logcat and then write the new one to the file
			try {
				Process process = Runtime.getRuntime().exec( "logcat -c");
				process = Runtime.getRuntime().exec( "logcat -f " + logFile + " *:V");
			} catch ( IOException e ) {
				e.printStackTrace();
			}

		} else if ( isExternalStorageReadable() ) {
			// only readable
		} else {
			// not accessible
		}
	}

	/* Checks if external storage is available for read and write */
	public boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		if ( Environment.MEDIA_MOUNTED.equals( state ) ) {
			return true;
		}
		return false;
	}

	/* Checks if external storage is available to at least read */
	public boolean isExternalStorageReadable() {
		String state = Environment.getExternalStorageState();
		if ( Environment.MEDIA_MOUNTED.equals( state ) ||
				Environment.MEDIA_MOUNTED_READ_ONLY.equals( state ) ) {
			return true;
		}
		return false;
	}
	
	public void handleUncaughtException (Thread thread, Throwable e)
	  {
		
	    e.printStackTrace(); // not all Android versions will print the stack trace automatically
	    Log.e("Stockem", e.getMessage());
	    
	    
	    Intent intent = new Intent (Intent.ACTION_SEND);
	    intent.setType ("plain/text");
	    intent.putExtra (Intent.EXTRA_EMAIL, new String[] {"log@mydomain.com"});
	    intent.putExtra (Intent.EXTRA_SUBJECT, "MyApp log file");
	    
	    intent.putExtra (Intent.EXTRA_TEXT, e.getMessage()+e.getLocalizedMessage()); // do this so some email clients don't complain about empty body.
	    
	    startActivity (intent);
	    
	  }
}
