package com.he.stockem;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.he.stockem.db.model.Customer;
import com.he.stockem.db.model.Order;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.DatabaseHelper;

public class UpdateCustomerLocation extends Fragment implements OnAsyncRequestComplete, LocationListener{

	Order order;
	DatabaseHelper controller;
	TextView txtUpdateLocCustomerName, txtUpdateLocDetails;
	Handler timeHandler = new Handler();
	long startTime = 0L;
	
	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;
	
	LocationManager locMgr;
	Location loc;
	String status="NA";
	Location location=null;
	Button btnUpdateCustomerLocation;
	boolean canGetLocation = false;
	
	
    double latitude; // latitude
    double longitude; // longitude
 
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10 meters
 
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 1 * 1; // 1 minute
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		View rootView = inflater.inflate(R.layout.fragment_update_customer_location, container, false);
		
		locMgr=(LocationManager)this.getActivity().getSystemService("location");
		boolean isGPSEnabled = locMgr
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
		boolean isNetworkEnabled = locMgr
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		
		if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
        } else {
            this.canGetLocation = true;
            // First get location from Network Provider
            if (isNetworkEnabled) {
            	locMgr.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                
                if (locMgr != null) {
                    location = locMgr
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }
                }
            }
            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (location == null) {
                	locMgr.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    
                    if (locMgr != null) {
                        location = locMgr
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
            }
        }
		txtUpdateLocCustomerName = (TextView) rootView.findViewById(R.id.txtUpdateLocCustomerName);
		txtUpdateLocDetails = (TextView) rootView.findViewById(R.id.txtUpdateLocDetails);
		btnUpdateCustomerLocation = (Button)rootView.findViewById(R.id.btnUpdateCustomerLocation);
		
		controller = DatabaseHelper.getInstance(getActivity());		
		order = controller.getOrder(getArguments().getString("localOrderID"));
		txtUpdateLocCustomerName.setText(order.getCustomer().getName());
		
		txtUpdateLocDetails.setText("LastUpdated:NA\nAccuracy:NA");
		
		startTime = SystemClock.uptimeMillis();
		
		timeHandler.postDelayed(updateTimerThread, 0);
		
		btnUpdateCustomerLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(location == null){
					Toast.makeText(getActivity(), "Location not available", 10).show();
					return;
				}
				
				Customer customer = order.getCustomer();
				customer.setLatitude(location.getLatitude());
				customer.setLongitude(location.getLongitude());
				customer.save();
				
				ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("module", "api"));
				params.add(new BasicNameValuePair("view", "updatecustomerloc"));
				params.add(new BasicNameValuePair("customerid", order.getCustomerID()));
				params.add(new BasicNameValuePair("latitude", String.valueOf(location.getLatitude())));
				params.add(new BasicNameValuePair("longitude", String.valueOf(location.getLongitude())));
				params.add(new BasicNameValuePair("accuracy", String.valueOf(location.getAccuracy())));
				String url=getResources().getString(R.string.server);
				AsyncRequest getPosts = new AsyncRequest(UpdateCustomerLocation.this, "GET", params,"updatecustomerlocation","Updating location.. Please wait");
				getPosts.execute(url);
			}
		});
		
		return rootView;
	}
	private Runnable updateTimerThread = new Runnable() {

		@SuppressLint("NewApi") public void run() {
			
			loc = locMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if(loc != null){
				
				location = loc;
				long diff=age_ms(location);
				
		        int secs = (int) (diff / 1000);
				int mins = secs / 60;
				secs = secs % 60;
				int milliseconds = (int) (updatedTime % 1000);
				
				status = String.valueOf("Last Location "+ mins + "m "+secs +"sec Ago\n"+"Accuracy:"+loc.getAccuracy());
		        
			}
			
			txtUpdateLocDetails.setText(status);
			timeHandler.postDelayed(this, 0);
		}

	};

	 @Override
	    public void onDestroyView() {
	        super.onDestroyView();
	        timeHandler.removeCallbacks(updateTimerThread);
	        locMgr.removeUpdates(UpdateCustomerLocation.this);

	    }

	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		if(label.contentEquals("updatecustomerlocation")){
			if(response.contentEquals("OK")){
				Toast.makeText(getActivity(), "Location updated", 10).show();
			}
			return;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
	public int age_minutes(Location last) {
	    return (int) (age_ms(last) / (60*1000));
	}

	public long age_ms(Location last) {
	    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
	        return age_ms_api_17(last);
	    return age_ms_api_pre_17(last);
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	private long age_ms_api_17(Location last) {
	    return (SystemClock.elapsedRealtimeNanos() - last
	            .getElapsedRealtimeNanos()) / 1000000;
	}

	private long age_ms_api_pre_17(Location last) {
	    return System.currentTimeMillis() - last.getTime();
	}
}
