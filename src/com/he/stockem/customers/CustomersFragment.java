package com.he.stockem.customers;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.he.stockem.R;
import com.he.stockem.db.model.AreaLine;
import com.he.stockem.db.model.Customer;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.CustomListAdapter;
import com.he.stockem.lib.CustomListItem;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.UtilityFunctions;

public class CustomersFragment extends Fragment implements OnAsyncRequestComplete, SearchView.OnQueryTextListener{
	
	public CustomersFragment(){}
	
	DatabaseHelper controller;
	
	
	UtilityFunctions util =new UtilityFunctions();
	private List<CustomListItem> customList = new ArrayList<CustomListItem>();
	private ListView listView;
	private CustomListAdapter adapter;
	private SearchView mSearchView;
	CustomListItem selectedItem;
	public Spinner spinCustomerFragCategory;
	SharedPreferences sharedPrefs;
	List<String> routeName = new ArrayList<String>();
	Button btnCustomerNew;
	String customerName="";
		
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_customers, container, false);
        customList.clear();
        mSearchView = (SearchView)rootView.findViewById(R.id.searchCustomerFrag);
        btnCustomerNew = (Button)rootView.findViewById(R.id.btnCustomerNew);
        controller = DatabaseHelper.getInstance(getActivity());    
        sharedPrefs = PreferenceManager
	            .getDefaultSharedPreferences(this.getActivity());
        
        List<AreaLine> area_lines = controller.getAllAreaLines();
        routeName.add("All");
        routeName.add("Nearby Stores");
        
        for(AreaLine area_line : area_lines){		
        	routeName.add(area_line.getName());
		}
        spinCustomerFragCategory = (Spinner)rootView.findViewById(R.id.spinCustomerFragCategory);
           	ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, routeName);    			    
		    categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
		    spinCustomerFragCategory.setAdapter(categoryAdapter);		    
		    spinCustomerFragCategory.setOnItemSelectedListener(new OnItemSelectedListener(){
	    		
				@Override
				public void onItemSelected(AdapterView<?> parent,
						View view, int position, long id) {
					// TODO Auto-generated method stub
					String item = parent.getItemAtPosition(position).toString();
					controller.insertUpdateConfig("last_cust", String.valueOf(position));
					mSearchView.setQuery("", false);
					mSearchView.clearFocus();
					new getCustomersTask(item).execute();
									
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub
					
				}
		    	
		    });
	    	
	    	
	            
        listView = (ListView) rootView.findViewById(R.id.listCustomerFrag);
		adapter = new CustomListAdapter(getActivity(), customList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				selectedItem = (CustomListItem)(parent.getItemAtPosition(position));				
				Bundle args = new Bundle();
				args.putString("customerID", String.valueOf(selectedItem.getID()));
				UpdateCustomer nextFrag= new UpdateCustomer();
				nextFrag.setArguments(args);
			     getActivity().getFragmentManager().beginTransaction()
			     .replace(R.id.frame_container, nextFrag)											     
			     .addToBackStack("Update Customer")
			     .commit();
				
			}
			
		});
		
		listView.setTextFilterEnabled(false);
		setupSearchView();
		
		if(!controller.getConfigValue("last_cust").isEmpty()){
			spinCustomerFragCategory.setSelection(Integer.parseInt(controller.getConfigValue("last_cust")));
	    }
		
		
		btnCustomerNew.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LayoutInflater layoutInflater = LayoutInflater.from(v.getContext());
				View promptView = layoutInflater.inflate(R.layout.add_customer_popup, null);
				final EditText etCustomerName = (EditText) promptView.findViewById(R.id.ETAddNewCustomerName);
				
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getContext());
				alertDialogBuilder.setView(promptView);
				
				alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("Create Customer", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {						
								
								
								if(etCustomerName.getText().toString().isEmpty()){
									Toast.makeText(getActivity(), "Please enter the customer name", 10).show();
									return;
								}
								customerName = etCustomerName.getText().toString();
								ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
								params.add(new BasicNameValuePair("name", etCustomerName.getText().toString()));
								String url=getResources().getString(R.string.server)+"?module=api/customers&view=addCustomer";
								AsyncRequest getPosts = new AsyncRequest(CustomersFragment.this, "POST", params,"add_customer","Please wait");
								getPosts.execute(url);	
							}
						})
				.setNegativeButton(getResources().getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,	int id) {
								dialog.cancel();
							}
						});

		// create an alert dialog
		AlertDialog alertD = alertDialogBuilder.create();

		alertD.show();	

				
			}
		});
        return rootView;
		
	}
	
	private void setupSearchView() {
		mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.setQueryHint("Search Here");
    }
	
	
	

	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		try {
			JSONObject obj = new JSONObject(response);
			if(obj.getBoolean("error")){
				Toast.makeText(getActivity(), obj.getString("message"), 10).show();
			}else{
				String customerID = obj.getString("customer_id");
				Customer customer = new Customer(getActivity());
				customer.setName(customerName);
				customer.setID(customerID);
				customer.setCustomField3(customerName);
				customer.setEnabled("1");
				String localDBID = customer.insert();
				Toast.makeText(getActivity(), "Customer added successfully", 10).show();
				
				CustomListItem listItem = new CustomListItem();
				listItem.setTitle(customerName);
				listItem.setID(customerID);				
				listItem.setHideDescription(true);
				customList.add(listItem);
				adapter.notifyDataSetChanged();
				
				Bundle args = new Bundle();
				args.putString("customerID", customerID);
				UpdateCustomer nextFrag= new UpdateCustomer();
				nextFrag.setArguments(args);
			     getActivity().getFragmentManager().beginTransaction()
			     .replace(R.id.frame_container, nextFrag)											     
			     .addToBackStack("Update Customer")
			     .commit();
			     
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getActivity(), "Unable to create the customer", 10).show();
		}
	
	}

	public boolean onQueryTextChange(String newText) {
		
		Filter filter = adapter.getFilter();
        if (TextUtils.isEmpty(newText)) {
            filter.filter("");
        } else {
            filter.filter(newText);
        }        
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }
    
        
    class getCustomersTask extends AsyncTask<String, Integer, String>  {
    	String route="All";    	
    	ProgressDialog pDialog;
        public getCustomersTask(String route) {
			// TODO Auto-generated constructor stub
        	this.route=route;
        	
		}
        public void onPreExecute() {
    		
    		pDialog = new ProgressDialog(CustomersFragment.this.getActivity());
    		pDialog.setMessage("Please wait"); // typically you will define such
    		// strings in a remote file.
    		pDialog.show();		
    	}
        
		public String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			List<Customer> customers=new ArrayList<Customer>();
			
			customList.clear();
			
			if(route.contentEquals("All")){   
				customers = controller.getAllCustomersByAreaLine(0);
			}else if(route.contentEquals("Nearby Stores")){
				customers = controller.getNearestCustomer(sharedPrefs.getString("prefBuffer", "500"));	  	    	      			    	    	  
	  	     }else{
	  	    	  AreaLine area_line = new AreaLine(getActivity());
	  	    	  area_line.setName(route);    			
	  	    	  customers = area_line.getCustomers();	  	    	  
	  	     }
			
			for(Customer customer : customers){								
				CustomListItem listItem = new CustomListItem();
				listItem.setTitle(customer.getName());
				listItem.setID(customer.getID());
				
				listItem.setHideDescription(true);
				customList.add(listItem);	
			}
			

			return "";
		}
		
		public void onPostExecute(String response) {			
			adapter.setOriginalData(customList);
			adapter.notifyDataSetChanged();
			pDialog.dismiss();
		}
    }
    
}
