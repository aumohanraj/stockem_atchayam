package com.he.stockem.db.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.he.stockem.lib.DatabaseHelper;



public class Config {
	
	String id="";
	String config_name="";
	String config_value="";
	
	public Config() {
			
	}	
		
	//Set Methods for all the attributes of the table
	public void setID(String id) {
		this.id = id;
	}
	public void setConfigName(String ConfigName) {
		this.config_name = ConfigName;
	}
	public void setConfigValue(String ConfigValue) {
		this.config_value = ConfigValue;
	}
	
	
	//Get Methods for all the attributes of the table
	public String getID() {
		return id;
	}	
	public String getConfigName() {
		return this.config_name ;
	}
	public String getConfigValue() {
		return this.config_value ;
	}
	
	public String save(Context context){
		SQLiteDatabase db = DatabaseHelper.getInstance(context).getDB();
		ContentValues values = new ContentValues();			
		values.put(DatabaseHelper.KEY_CONFIG_CONFIGNAME, this.config_name);
		values.put(DatabaseHelper.KEY_CONFIG_CONFIGVALUE, this.config_value);
		
		if(this.id.contentEquals("")){
			this.id=String.valueOf((int)db.insert(DatabaseHelper.TABLE_CONFIG, null, values));
			return this.id;			
		}else{
			db.update(DatabaseHelper.TABLE_CONFIG, values, "id=?", new String[] {this.id});
			return this.id;
		}
	}
	
}
