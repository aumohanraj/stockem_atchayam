package com.he.stockem.db.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.UtilityFunctions;


public class Order {
	
	String id="";
	String index_id="";
	String domain_id="";
	String biller_id="";
	String customer_id="";
	String type_id="";
	String preference_id="";
	String date="";
	String custom_field1="";
	String custom_field2="";
	String custom_field3="";
	String custom_field4="";
	String note="";
	String salesman="";
	String status="";
	String invoice_id="";
	List<OrderItem> Items = new ArrayList<OrderItem>();   
	Context context;
	
	public static final String KEY_TYPE_ORDER="2";
	public static final String KEY_TYPE_INVOICE="5";
	public static final String KEY_TYPE_PURCHASE_ORDER="6";

	public Order(Context context) {
		this.context=context;
	}
	
	//Set Methods for all the attributes of the table
	public void setID(String id) {
		this.id = id;
	}	
	public void setIndexID(String indexID){
		this.index_id=indexID;
	}
	public void setDomainID(String domainID){
		this.domain_id=domainID;		
	}
	public void setBillerID(String billerID){
		this.biller_id=billerID;
	}
	public void setCustomerID(String CustomerID){
		this.customer_id=CustomerID;
	}
	public void setTypeID(String TypeID){
		this.type_id=TypeID;
	}
	public void setPreferenceID(String PreferenceID){
		this.preference_id=PreferenceID;
	}
	public void setDate(String Date){
		this.date=Date;
	}
	public void setCustomField1(String CustomField1){
		this.custom_field1=CustomField1;
	}
	public void setCustomField2(String CustomField2){
		this.custom_field2=CustomField2;
	}
	public void setCustomField3(String CustomField3){
		this.custom_field3=CustomField3;
	}
	public void setCustomField4(String CustomField4){
		this.custom_field4=CustomField4;
	}
	public void setNote(String Note){
		this.note=Note;
	}
	public void setSalesman(String Salesman){
		this.salesman=Salesman;
	}
	public void setStatus(String Status){
		this.status=Status;
	}
	public void setInvoiceID(String InvoiceID){
		this.invoice_id=InvoiceID;
	}		
	
	//Get Methods for all the attributes of the table
	public String getID() {
		return id;
	}	
	public String getIndexID(){
		return this.index_id;
	}
	public String getDomainID(){
		return this.domain_id;		
	}
	public String getBillerID(){
		return this.biller_id;
	}
	public String getCustomerID(){
		return this.customer_id;
	}
	public String getTypeID(){
		return this.type_id;
	}
	public String getPreferenceID(){
		return this.preference_id;
	}
	public String getDate(){
		return this.date;
	}
	public String getCustomField1(){
		return this.custom_field1;
	}
	public String getCustomField2(){
		return this.custom_field2;
	}
	public String getCustomField3(){
		return this.custom_field3;
	}
	public String getCustomField4(){
		return this.custom_field4;
	}
	public String getNote(){
		return this.note;
	}
	public String getSalesman(){
		return this.salesman;
	}
	public String getStatus(){
		return this.status;
	}
	public String getInvoiceID(){
		return this.invoice_id;
	}
	
	public Double getOrderTotal(){
		Double total=0.00;
		for(OrderItem i: getItems()){
			total = total +i.getTotal();
		}		
		UtilityFunctions util =new UtilityFunctions(context);
		return util.roundTwoDecimals(total);
	}
	
	public int getOrderItemTotalQuantity(String productID){
		int quantity=0;
		
		for(OrderItem i: getItems()){			
			if(String.valueOf(i.getProductID()).trim().contentEquals(productID.trim())){
				quantity = quantity+i.getQuantity();
				quantity = quantity+i.getFreeQty(); 
			}
		}		
		
		return quantity;
	}
	public Customer getCustomer(){
		return DatabaseHelper.getInstance(context).getCustomer(this.customer_id);
	}
	
	public Supplier getSupplier(){
		return DatabaseHelper.getInstance(context).getSupplier(this.customer_id);
	}
	
	public List<OrderItem> getItems(){
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		Cursor cursor = db.query(DatabaseHelper.TABLE_ORDER_ITEM, new String[]{"id"}, DatabaseHelper.KEY_ORDERITEM_ORDER_ID+"=?", new String[] {this.id}, null, null, null);
		if(cursor.moveToFirst()){
			Items.clear();
			do{
				Items.add(controller.getOrderItem(cursor.getString(cursor.getColumnIndex(DatabaseHelper.KEY_ORDERITEM_ID))));
			}while(cursor.moveToNext());
		}
		DatabaseHelper.closeCursor(cursor);
		return this.Items;
	}
	
	public String save(){
		DatabaseHelper controller = DatabaseHelper.getInstance(context);
		SQLiteDatabase db = controller.getDB();
		ContentValues values = new ContentValues();	
		if(this.id.contentEquals("")){
			this.setDomainID(controller.getConfigValue("domain_id"));
			if(this.getIndexID().isEmpty()){
				this.setIndexID("0");
			}			
			this.setBillerID(controller.getConfigValue("biller_id"));			
			this.setPreferenceID("1");
			this.setInvoiceID("0");
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();		
			this.setDate(dateFormat.format(date));
		}
		
		if(this.getBillerID().isEmpty() || this.getBillerID() == null){
			return "0";
		}
		values.put(DatabaseHelper.KEY_ORDER_BILLER_ID, biller_id);
		values.put(DatabaseHelper.KEY_ORDER_CUSTOM_FIELD1, custom_field1);
		values.put(DatabaseHelper.KEY_ORDER_CUSTOM_FIELD2, custom_field2);
		values.put(DatabaseHelper.KEY_ORDER_CUSTOM_FIELD3, custom_field3);
		values.put(DatabaseHelper.KEY_ORDER_CUSTOM_FIELD4, custom_field4);
		values.put(DatabaseHelper.KEY_ORDER_CUSTOMER_ID, customer_id);
		values.put(DatabaseHelper.KEY_ORDER_DATE, date);
		values.put(DatabaseHelper.KEY_ORDER_DOMAIN_ID, domain_id);
		values.put(DatabaseHelper.KEY_ORDER_INDEX_ID, index_id);
		values.put(DatabaseHelper.KEY_ORDER_INVOICE_ID, invoice_id);
		values.put(DatabaseHelper.KEY_ORDER_NOTE, note);
		values.put(DatabaseHelper.KEY_ORDER_PREFERENCE_ID, preference_id);
		values.put(DatabaseHelper.KEY_ORDER_SALESMAN, salesman);
		values.put(DatabaseHelper.KEY_ORDER_STATUS, status);
		values.put(DatabaseHelper.KEY_ORDER_TYPE_ID, type_id);
		
		if(this.id.contentEquals("")){
			this.id=String.valueOf((int)db.insert(DatabaseHelper.TABLE_ORDER, null, values));			
			return this.id;			
		}else{
			db.update(DatabaseHelper.TABLE_ORDER, values, "id=?", new String[] {this.id});
			return this.id;
		}
	}
	
	public String addItem(OrderItem item){
		item.setOrderID(this.id);
		item.save();
		Items.add(item);
		return item.getID();
	}
	
	
}

