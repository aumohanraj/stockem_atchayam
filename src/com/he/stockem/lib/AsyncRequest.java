package com.he.stockem.lib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.he.stockem.R;
 
public class AsyncRequest extends AsyncTask<String, Integer, String> {
	OnAsyncRequestComplete caller;
	Context context;
	Fragment fragment;
	String method = "GET";
	List<NameValuePair> parameters = null;
	ProgressDialog pDialog = null;
	String label="";
	String message="";
	DatabaseHelper controller;
	String id="";
	SharedPreferences sharedPrefs;
 // Three Constructors
	public AsyncRequest(Activity a, String m, List<NameValuePair> p, String label, String Message) {
		caller = (OnAsyncRequestComplete) a;
		context = a;
		method = m;
		parameters = p;
		this.label=label;
		this.message=Message;
		controller=DatabaseHelper.getInstance(context);
		sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
	}
	
	public AsyncRequest(Fragment f, String m, List<NameValuePair> p, String label, String Message) {
		caller = (OnAsyncRequestComplete) f;
		fragment = f;
		context = f.getActivity();
		method = m;
		parameters = p;
		this.label=label;
		this.message=Message;
		controller=DatabaseHelper.getInstance(context);
		sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
	}
	
	public AsyncRequest(Activity a, String m) {
		caller = (OnAsyncRequestComplete) a;
		context = a;
		method = m;		
		sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
	}
	
	public AsyncRequest(Activity a) {
		caller = (OnAsyncRequestComplete) a;
		context = a;	
		sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
	}
 
	

	public interface OnAsyncRequestComplete {
		
		public void asyncResponse(String response,String label);

	
	}
	
	public String doInBackground(String... urls) {
		// get url pointing to entry point of API
		
		if(!isNetworkAvailable()){
			return "network not available";
		}
		
		
		
		String address = urls[0].toString();
		if(!sharedPrefs.getString("prefURL", "").isEmpty()){
			address = address.replace("http://10.2.100.186/simpleinvoice/index.php", sharedPrefs.getString("prefURL", ""));
		}
		
		
		if (method == "POST") {
			return post(address);
			}
		
		if (method == "GET") {
			return get(address);
			
			}
		return null;
	}
 
	public void onPreExecute() {
		
		pDialog = new ProgressDialog(context);
		pDialog.setMessage(message); // typically you will define such
		// strings in a remote file.
		pDialog.show();		
	}
 
	public void onProgressUpdate(Integer... progress) {
		// you can implement some progressBar and update it in this record
		// setProgressPercent(progress[0]);
	}
	
	public void onPostExecute(String response) {
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
			}
		if(response == null){
			Toast.makeText(context, "Try Again later!!!", 10).show();
			return;
		}
		
		Log.i("AsyncRequest","Response:"+response);
		if(response.contentEquals("network not available") && label.contentEquals("getprice")){
			caller.asyncResponse(response,label);
			return;
		}
		if(response.contentEquals("network not available")){
			Toast.makeText(context, "No Internet connection", 10).show();
			return;
		}
				
		
		if(response.toLowerCase().contains("login req") && !label.toLowerCase().contains("sessioncheck")){
			Toast.makeText(context, "Login Required", 10).show();						
			return;
		}
		
		if(response.toLowerCase().contains("you are not allowed to view this page") && !label.toLowerCase().contains("sessioncheck")){
			Toast.makeText(context, "You are not allowed to view this page", 10).show();						
			return;
		}
		
		
		
		caller.asyncResponse(response,label);
	}
	
	protected void onCancelled(String response) {
		if (pDialog != null && pDialog.isShowing()) {
			pDialog.dismiss();
			}
		if(response==null){
			response="";
		}
		caller.asyncResponse(response,label);
	}
	
	@SuppressWarnings("deprecation")
	private String get(String address) {
		try {
			if (parameters != null) {
				String query = "";
				String EQ = "="; String AMP = "&";
				for (NameValuePair param : parameters) {
					query += param.getName() + EQ + URLEncoder.encode(param.getValue()) + AMP;
					if(param.getName().contentEquals("id")){
						id=param.getValue();
					}
					}
				
				if (query != "") {
					address += "?" + query;
					}
				}
			
			final HttpParams httpParams = new BasicHttpParams();
		    HttpConnectionParams.setConnectionTimeout(httpParams, 120000);
		    
			HttpClient client = new DefaultHttpClient(httpParams);	
			if(label.contentEquals("login")){				
				controller.insertUpdateConfig("cookie", "");
			}
			client=setCookie(client); 
			HttpGet get= new HttpGet(address);
			HttpResponse response = client.execute(get);
			if(label.contentEquals("login")){				
				updateCookie(client);
			}
			
			if(label.contentEquals("getpdf")){

		        String fileName ="invoice"+id+".html";  
		        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
		        File folder = new File(extStorageDirectory, "invoices");
		        folder.mkdir();

		        File pdfFile = new File(folder, fileName);

		        try{
		            pdfFile.createNewFile();
		        }catch (IOException e){
		        	Log.e("AsyncRequest", "getpdf", e);
		            e.printStackTrace();
		        }
				InputStream inputStream = response.getEntity().getContent();
	            FileOutputStream fileOutputStream = new FileOutputStream(pdfFile);
	            final int  MEGABYTE = 1 * 1024;
	            byte[] buffer = new byte[MEGABYTE];
	            int bufferLength = 0;
	            while((bufferLength = inputStream.read(buffer))>0 ){
	                fileOutputStream.write(buffer, 0, bufferLength);
	            }
	            fileOutputStream.close();
				return fileName;				
			}else{
				return stringifyResponse(response);
			}
							
			
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			Log.e("AsyncRequest", "get", e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("AsyncRequest", "get", e);
			}
		return null;		
	}
	
	private String post(String address) {
		try {
			final HttpParams httpParams = new BasicHttpParams();
		    HttpConnectionParams.setConnectionTimeout(httpParams, 120000);
		    
			HttpClient client = new DefaultHttpClient(httpParams);
			HttpPost post = new HttpPost(address);
			
			if(label.contentEquals("login")){				
				controller.insertUpdateConfig("cookie", "");
			}
			if (parameters != null) {
				post.setEntity(new UrlEncodedFormEntity(parameters));
			}
			client=setCookie(client);
			HttpResponse response = client.execute(post);
			if(label.contentEquals("login")){				
				updateCookie(client);
			}			
			return stringifyResponse(response);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			Log.e("AsyncRequest", "post", e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("AsyncRequest", "post", e);
			e.printStackTrace();
		}
		return null;	
	
	}
	
	private String stringifyResponse(HttpResponse response) {
		int statusCode = response.getStatusLine().getStatusCode();
		Log.i("AsyncRequest","Status Code"+String.valueOf(statusCode));
		BufferedReader in;		
		try {
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			in.close();
			return sb.toString();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			Log.e("AsyncRequest", "stringifyResponse", e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("AsyncRequest", "stringifyResponse", e);
		}
		return null;
	}
	
	public static BasicCookieStore getCookieStore(String cookies, String domain) {
	    String[] cookieValues = cookies.split(";");
	    BasicCookieStore cs = new BasicCookieStore();

	    BasicClientCookie cookie;
	    for (int i = 0; i < cookieValues.length; i++) {
	        String[] split = cookieValues[i].split("=");
	        if (split.length == 2)
	            cookie = new BasicClientCookie(split[0], split[1]);
	        else
	            cookie = new BasicClientCookie(split[0], null);
	        cookie.setDomain(domain);
	        cs.addCookie(cookie);
	    }
	    return cs;
	}
	
	public void updateCookie(HttpClient client) {
		
		List<Cookie> cookielist = ((AbstractHttpClient) client).getCookieStore().getCookies();
		String cookieText="";
		
		for (Cookie cookie : cookielist) {
			cookieText = cookie.getName()+"="+cookie.getValue()+";";
		}
		controller.insertUpdateConfig("cookie", cookieText);		
	}
	
	public HttpClient setCookie(HttpClient client){		
		BasicCookieStore lCS = getCookieStore(controller.getConfigValue("cookie"), context.getResources().getString(R.string.domain));
		if(sharedPrefs.getString("prefURL", "").contains("10.2.100.117")){
			lCS = getCookieStore(controller.getConfigValue("cookie"), "10.2.100.117");
		}		
		((AbstractHttpClient) client).setCookieStore(lCS);
		return client;
	}
	
	public boolean isNetworkAvailable() {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}
