package com.he.stockem.lib;

public class AutoCompleteItem {

    public String objectName;
    public int id;
    

    // constructor for adding sample data
    public AutoCompleteItem(String objectName, int id){    	
    	this.objectName = objectName;
    	this.id=id;
    }
    
    public int getID(){
    	return this.id;
    }
    
    public String getName(){
    	return this.objectName;
    }

}