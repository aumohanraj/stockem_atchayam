package com.he.stockem.lib;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.he.stockem.R;

public class AutocompleteArrayAdapter extends ArrayAdapter<AutoCompleteItem> {

	final String TAG = "AutocompleteCustomArrayAdapter.java";
		
    Context mContext;
    int layoutResourceId;
    ArrayList<AutoCompleteItem> data = null;
    private ArrayList<AutoCompleteItem> items;
    private ArrayList<AutoCompleteItem> itemsAll;
    private ArrayList<AutoCompleteItem> suggestions;

    public AutocompleteArrayAdapter(Context mContext, int layoutResourceId, ArrayList<AutoCompleteItem> listobj) {

        super(mContext, layoutResourceId, listobj);
        
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = listobj;
        this.items = listobj;
        this.itemsAll = (ArrayList<AutoCompleteItem>) items.clone();
        this.suggestions = new ArrayList<AutoCompleteItem>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	
    	try{
    		
	        /*
	         * The convertView argument is essentially a "ScrapView" as described is Lucas post 
	         * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
	         * It will have a non-null value when ListView is asking you recycle the row layout. 
	         * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
	         */
	        if(convertView==null){
	            // inflate the layout
	        	//LayoutInflater inflater = ((OrdersActivity) mContext).getLayoutInflater();
	        	LayoutInflater inflater = LayoutInflater.from(mContext);
	            convertView = inflater.inflate(layoutResourceId, parent, false);
	        }
	        
	        // object item based on the position
	        AutoCompleteItem objectItem = data.get(position);

	        // get the TextView and then set the text (item name) and tag (item ID) values
	        TextView textViewItem = (TextView) convertView.findViewById(R.id.textViewItem);
	        textViewItem.setText(objectItem.getName());
	        
	        // in case you want to add some style, you can do something like:
	        //textViewItem.setBackgroundColor(Color.CYAN);
	        
	    } catch (NullPointerException e) {
	    	Log.e("AutocompleteArrayAdapter", "getView", e);
		} catch (Exception e) {
			Log.e("AutocompleteArrayAdapter", "getView", e);
		}
    	
        return convertView;
        
    }
    
    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((AutoCompleteItem)(resultValue)).getName(); 
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (AutoCompleteItem customer : itemsAll) {
                    if(customer.getName().toLowerCase().contains(constraint.toString().toLowerCase())){
                        suggestions.add(customer);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected synchronized void  publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<AutoCompleteItem> filteredList = (ArrayList<AutoCompleteItem>) results.values;
            if(results != null && results.count > 0) {
                clear();         
                Iterator<AutoCompleteItem> iterator = filteredList.iterator();
                try{                
                while(iterator.hasNext()){
                	AutoCompleteItem c = iterator.next();
                	if(c== null || c.getID()==0){
                		iterator.remove();
                	}
                	add(c);
                }
                }catch(Exception e){
                	Log.e("AutocompleteArrayAdapter", "publishResults", e);
                }
                notifyDataSetChanged();
            }
        }
    };
}