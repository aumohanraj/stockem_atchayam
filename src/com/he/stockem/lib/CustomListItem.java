package com.he.stockem.lib;


public class CustomListItem {
	public String title, description, list_footer_left,list_footer_right,id,prop1,prop2,prop3,prop4;	
	public boolean hideDescription = false;
	public CustomListItem() {
	}

	public CustomListItem(String title, String description, String list_footer_left, String list_footer_right, String id) {
		this.title = title;
		this.description = description;
		this.list_footer_left = list_footer_left;
		this.list_footer_right = list_footer_right;
		this.id=id;
	}
	
	public String getID() {
		return id;
	}

	public void setID(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getFooterLeft() {
		return this.list_footer_left;
	}

	public void setFooterLeft(String footerLeft) {
		this.list_footer_left = footerLeft;
	}
	
	public String getFooterRight() {
		if(this.list_footer_right==null){
			this.list_footer_right="";
		}
		return this.list_footer_right;
	}

	public void setFooterRight(String footerRight) {
		this.list_footer_right = footerRight;
	}
	
	public String getProp1() {
		return this.prop1;
	}

	public void setProp1(String prop1) {
		this.prop1 = prop1;
	}
	
	public String getProp2() {
		return this.prop2;
	}

	public void setProp2(String prop2) {
		this.prop2 = prop2;
	}
	
	public String getProp3() {
		return this.prop3;
	}

	public void setProp3(String prop3) {
		this.prop3 = prop3;
	}
	
	public String getProp4() {
		return this.prop4;
	}

	public void setProp4(String prop4) {
		this.prop4 = prop4;
	}

	public boolean getHideDescription() {
		return hideDescription;
	}
	
	public void setHideDescription(boolean hideDescription) {
		this.hideDescription = hideDescription;
	}	
	
	
}
