package com.he.stockem.lib;

import java.text.DecimalFormat;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.he.stockem.R;

public class UtilityFunctions {
	Context context;
	public UtilityFunctions(){
		
	}
	
	public UtilityFunctions(Context c){
		this.context=c;
	}
	
	public double roundTwoDecimals(double d) {
	    DecimalFormat twoDForm = new DecimalFormat("#.##");
	    return Double.valueOf(twoDForm.format(d));
	}
	
	public String StringroundTwoDecimals(String value) {
	    DecimalFormat twoDForm = new DecimalFormat("#.##");
	    double d=0.00;
	    try{
	    	d = Double.parseDouble(value);
	    }catch(Exception e){
	    	Log.e("UtilityFunctions", "StringroundTwoDecimals",e);
	    }
	    return String.valueOf(twoDForm.format(d));
	}
	
	public ProgressDialog createProgressDialog(String text){
		ProgressDialog prgDialog;
		prgDialog = new ProgressDialog(this.context);
		prgDialog.setMessage(text);
		prgDialog.setCancelable(false);			
		return prgDialog;		
	}
	
	public TextView createTextView(String text){
		TextView tv = new TextView(this.context);
		  tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				  LayoutParams.WRAP_CONTENT));
		  tv.setBackgroundResource(R.drawable.cell_shape);
		  tv.setPadding(5, 5, 5, 5);
		  tv.setText(text);
		  return tv;
	}
	
	public String appendSpaces(String text, int length){
		
		String padded = String.format("%-20s", text);
		Log.i("UtilityFunctions", "appendSpaces"+padded);
		return padded;
	}
	
	public JSONArray getResults(Cursor cursor){
		JSONArray resultSet     = new JSONArray(); 
	
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
	
		            int totalColumn = cursor.getColumnCount();
		            JSONObject rowObject = new JSONObject();
	
		            for( int i=0 ;  i< totalColumn ; i++ )
		            {
		                if( cursor.getColumnName(i) != null ) 
		                { 
		                    try 
		                    { 
		                        if( cursor.getString(i) != null )
		                        {
		                        
		                            rowObject.put(cursor.getColumnName(i) ,  cursor.getString(i) );
		                        }
		                        else
		                        {
		                            rowObject.put( cursor.getColumnName(i) ,  "" ); 
		                        }
		                    }
		                    catch( Exception e )
		                    {
		                        
		                    }
		                } 
		            } 
		            resultSet.put(rowObject);
		            cursor.moveToNext();
		        } 
		        cursor.close(); 
		        Log.d("TAG_NAME", resultSet.toString() );
		        return resultSet;  
	}

}
