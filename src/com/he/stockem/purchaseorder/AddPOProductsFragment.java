package com.he.stockem.purchaseorder;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.he.stockem.R;
import com.he.stockem.db.model.Order;
import com.he.stockem.db.model.Supplier;
import com.he.stockem.lib.AsyncRequest;
import com.he.stockem.lib.AsyncRequest.OnAsyncRequestComplete;
import com.he.stockem.lib.CustomListAdapter;
import com.he.stockem.lib.CustomListItem;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.UtilityFunctions;

public class AddPOProductsFragment extends Fragment implements OnAsyncRequestComplete,  SearchView.OnQueryTextListener{
	
	public AddPOProductsFragment(){}
	String customerName,customerID;
	
	ProgressDialog prgDialog;
	DatabaseHelper controller;
	
	EditText ETRecentProdQuantity,ETRecentProdQuantityFree, ETRecentProdUnitPrice;	
	TextView txtRecentCustomerName, TxtRecentProdUnitPrice;
	Order order;
	UtilityFunctions util =new UtilityFunctions();
	private List<CustomListItem> customList = new ArrayList<CustomListItem>();
	private ListView listView;
	private CustomListAdapter adapter;
	private SearchView mSearchView;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_recent, container, false);
        
        controller = DatabaseHelper.getInstance(getActivity());
        order = controller.getOrder(getArguments().getString("localOrderID"));
        
        if(order.getID().isEmpty()){        
        	getActivity().onBackPressed();
        	return rootView;
        }
        mSearchView = (SearchView)rootView.findViewById(R.id.search_view);
        
        txtRecentCustomerName=(TextView)rootView.findViewById(R.id.txtRecentCustomerName);
        
        listView = (ListView) rootView.findViewById(R.id.listRecentProducts);
		adapter = new CustomListAdapter(getActivity(), customList);
		listView.setAdapter(adapter);
		
		listView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final CustomListItem selectedItem = (CustomListItem)(parent.getItemAtPosition(position));
				LayoutInflater layoutInflater = LayoutInflater.from(view.getContext());
				View promptView = layoutInflater.inflate(R.layout.add_recent_product_popup, null);
				TextView tv = (TextView)promptView.findViewById(R.id.TxtPlus);
				tv.setVisibility(View.GONE);
				String pending_order_text = selectedItem.getFooterLeft();
				ETRecentProdQuantity = (EditText)promptView.findViewById(R.id.ETRecentProdQuantity);
				ETRecentProdQuantityFree = (EditText)promptView.findViewById(R.id.ETRecentProdQuantityFree);
				ETRecentProdQuantityFree.setVisibility(View.GONE);
				ETRecentProdUnitPrice = (EditText)promptView.findViewById(R.id.ETRecentProdUnitPrice);
				ETRecentProdUnitPrice.setVisibility(View.GONE);
				TxtRecentProdUnitPrice = (TextView)promptView.findViewById(R.id.TxtRecentProdUnitPrice);
				TxtRecentProdUnitPrice.setVisibility(View.GONE);
				final String quantity= selectedItem.getProp1();
				final String free= selectedItem.getProp2();
								
				ETRecentProdQuantity.addTextChangedListener(new TextWatcher(){					   
					   @Override
					   public void afterTextChanged(Editable s) {
						   String changedText=s.toString();
						   if(changedText.isEmpty() || changedText.contentEquals("0") || quantity.contentEquals("0")||free.contentEquals("0") || quantity.isEmpty()||free.isEmpty()){
							   return;
						   }else{
							   int int_quantity= Integer.parseInt(quantity);
							   int int_free= Integer.parseInt(free);
							   int int_text = Integer.parseInt(changedText);
							   int_text = int_text - (int_text%int_quantity);
							   ETRecentProdQuantityFree.setText(String.valueOf((int)(int_text/int_quantity)*int_free)); 
						   }
					   }
					 		
					   @Override    
					   public void beforeTextChanged(CharSequence s, int start,
					     int count, int after) {
					   }

					   @Override    
					   public void onTextChanged(CharSequence s, int start,
					     int before, int count) {
					    
					    
					   }
					
				});
				
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						view.getContext());
				alertDialogBuilder.setView(promptView);
				alertDialogBuilder.setTitle(selectedItem.getTitle());
				alertDialogBuilder
				.setMessage(pending_order_text)
				.setCancelable(false)
				.setPositiveButton(view.getContext().getResources().getString(R.string.save),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						if(ETRecentProdQuantity.getText().toString().trim().contentEquals("")||ETRecentProdQuantity.getText().toString().trim().contentEquals("0")){
							Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.quantity_required), 10).show();
							return;
						}						
						String orderID=getArguments().getString("localOrderID");
						Cursor prodCursor=controller.getProduct(String.valueOf(selectedItem.getID()));
						
						if(prodCursor==null){
							Toast.makeText(getActivity(), "Selected product is not available. Please sync and try again!!!", 10).show();
							return;
						}
						int prodFreeInsertID=0;
						int prodFreeTaxInertID=0;
						if(!ETRecentProdQuantityFree.getText().toString().trim().contentEquals("")&& !ETRecentProdQuantityFree.getText().toString().trim().contentEquals("0")){
							prodFreeInsertID=controller.insertProduct(orderID, ETRecentProdQuantityFree.getText().toString().trim(), String.valueOf(selectedItem.getID()), "0", "0");
							prodFreeTaxInertID=controller.insertProductTax(String.valueOf(prodFreeInsertID), ETRecentProdQuantityFree.getText().toString(), selectedItem.getID(), "0", "0", prodCursor.getString(prodCursor.getColumnIndex("tax_id")));
						}
						
						String tax_id=prodCursor.getString(prodCursor.getColumnIndex("tax_id"));
						String cost = selectedItem.getDescription();
						String quantity = ETRecentProdQuantity.getText().toString();
						String tax_percentage = prodCursor.getString(prodCursor.getColumnIndex("tax_percentage"));
						
						if(cost.toString().trim().contentEquals("")||cost.isEmpty()){									
							cost="-1";
						}else{											
							double dbl_cost= Double.parseDouble(cost);
							double dbl_tax_percentage= Double.parseDouble(tax_percentage);
							double dbl_unit_price = (dbl_cost/(1+(dbl_tax_percentage/100)));
							cost = String.valueOf(dbl_unit_price);											
						}
						
						int prodInsertID=controller.insertProduct(orderID, quantity, selectedItem.getID(), cost, tax_percentage);
						int prodTaxInertID=controller.insertProductTax(String.valueOf(prodInsertID), quantity, selectedItem.getID(), cost, tax_percentage, tax_id);
						if(prodFreeInsertID != 0){
							controller.updateOrderItemFree(String.valueOf(prodInsertID), String.valueOf(prodFreeInsertID), ETRecentProdQuantityFree.getText().toString());
						}
						selectedItem.setFooterRight("Added("+order.getOrderItemTotalQuantity(selectedItem.getID())+")");
						adapter.notifyDataSetChanged();
						dialog.cancel();
					}
				  })
				.setNegativeButton(getActivity().getResources().getString(R.string.cancel),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {	
						dialog.cancel();								
					}
				});
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();				
			}
			
		});
		if(order==null){
			Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.unable_to_get_order), 10).show();			
		}else{
			txtRecentCustomerName.setText(order.getSupplier().getName());
			getRecentProducts(order.getCustomerID());			
		}		
		setupSearchView();
        return rootView;
		
	}
	
	private void setupSearchView() {
		mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.setQueryHint("Search Here");
    }

	public void getRecentProducts(String supplier_id) {
		// TODO Auto-generated method stub
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("module", "api/orders"));
		params.add(new BasicNameValuePair("view", "supplierproducts"));
		
		params.add(new BasicNameValuePair("qtype", "supplier"));
		Supplier supplier=controller.getSupplier(supplier_id);
		params.add(new BasicNameValuePair("supplier_id", supplier.getName()));
		params.add(new BasicNameValuePair("query", supplier.getName()));
		
		String url=getResources().getString(R.string.server);
		AsyncRequest getPosts = new AsyncRequest(this, "GET", params,"getrecentproducts","Getting products");
		getPosts.execute(url);
	}

	@Override
	public void asyncResponse(String response, String label) {
		// TODO Auto-generated method stub
		System.out.println(response);
		JSONObject response_obj;
				
		try {
			response_obj = new JSONObject(response);
			JSONArray productsJSON=response_obj.getJSONArray("products");
			
			if(productsJSON.length()==0){				
				Toast.makeText(getActivity(), "No products for this customer", 10).show();
				return;
			}
			customList.clear();
			for(int i=0; i<productsJSON.length();i++){
				JSONObject obj = (JSONObject)productsJSON.get(i);				
				CustomListItem listItem = new CustomListItem();
				listItem.setTitle(obj.getString("description")+","+obj.getString("custom_field1"));
				listItem.setFooterLeft("Available "+obj.getString("quantity").replace(".000000", "") + ", Order "+obj.getString("pending_order_quantity").replace(".000000", ""));
				listItem.setFooterRight("");
				listItem.setID(obj.getString("id"));
				listItem.setProp1(obj.getString("free_offer_qty"));
				listItem.setProp2(obj.getString("free_qty"));
				String unit_price = obj.getString("unit_price");
				if(unit_price.contentEquals("null")||unit_price.trim().contentEquals("")){
					unit_price =obj.getString("default_unit_price");
				}		
				if(unit_price.trim().contentEquals("")||unit_price.isEmpty()){
					unit_price="0";
				}
				String tax_percentage = controller.getTaxPercentage(obj.getString("default_tax_id"));
				double dbl_cost= Double.parseDouble(unit_price);
				double dbl_tax_percentage= Double.parseDouble(tax_percentage);
				double dbl_unit_price = dbl_cost + (dbl_cost*(dbl_tax_percentage/100));
				unit_price = String.valueOf(util.roundTwoDecimals(dbl_unit_price));
				listItem.setDescription(unit_price);
				int added_quantity= order.getOrderItemTotalQuantity(listItem.getID());
				if(added_quantity != 0){
					listItem.setFooterRight("Added(" +String.valueOf(added_quantity)+")");
				}
				customList.add(listItem);				
			}
			adapter.notifyDataSetChanged();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e("Add PO Products","asyncresponse",e);
			
		}
	}
	
	public boolean onQueryTextChange(String newText) {
		
		Filter filter = adapter.getFilter();
        if (TextUtils.isEmpty(newText)) {
            filter.filter("");
        } else {
            filter.filter(newText);
        }
        
        
        return true;
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }
}
