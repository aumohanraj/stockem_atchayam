package com.he.stockem.purchaseorder;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.Toast;

import com.he.stockem.R;
import com.he.stockem.db.model.Order;
import com.he.stockem.db.model.Supplier;
import com.he.stockem.lib.AutoCompleteItem;
import com.he.stockem.lib.AutocompleteArrayAdapter;
import com.he.stockem.lib.CustomListAdapter;
import com.he.stockem.lib.CustomListItem;
import com.he.stockem.lib.DatabaseHelper;
import com.he.stockem.lib.UtilityFunctions;

public class PurchaseOrderFragment extends Fragment {
	
	public PurchaseOrderFragment(){}
	
	TableLayout tblOrders;
	Button btnOrdersNew;	
	DatabaseHelper controller;;
	ArrayAdapter<AutoCompleteItem> customerAdapter;	
	int selectedCustomerID=0;
	String selectedCustomerName="";	
	UtilityFunctions util;
	private List<CustomListItem> customList = new ArrayList<CustomListItem>();	
	private ListView listView;
	private CustomListAdapter adapter;
	ArrayList<AutoCompleteItem> customerArray;
	List<String> sIds = new ArrayList<String>();
	List<String> names = new ArrayList<String>();	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_purchase_orders, container, false);
        controller = DatabaseHelper.getInstance(getActivity());
        
        
        
        for(Supplier supplier : controller.getAllSupplier()){
  		  names.add(supplier.getName());
  		  sIds.add(supplier.getId());
  	   }
        
        listView = (ListView) rootView.findViewById(R.id.listPurchaseOrder);
		adapter = new CustomListAdapter(getActivity(), customList);
				
		listView.setAdapter(adapter);
		
		listView.setOnItemLongClickListener(new OnItemLongClickListener(){

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				final CustomListItem selectedItem = (CustomListItem)(parent.getItemAtPosition(position));
				
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());
				alertDialogBuilder.setTitle("Confirm Delete");
				alertDialogBuilder.setMessage("Do you want to delete the order of " + selectedItem.getTitle());
				alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {						
						controller.deleteOrder(selectedItem.getID());
						Toast.makeText(getActivity(), "Order Deleted", 10).show();
						BuildOrderTable();
					}
				});
				
				alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {						
						dialog.dismiss();
					}
				});
				
				alertDialogBuilder.show();
				return true;
				
			}		
			
		});
				
				
		listView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				CustomListItem selectedItem = (CustomListItem)(parent.getItemAtPosition(position));
				
				Bundle args = new Bundle();
				args.putString("localOrderID", String.valueOf(selectedItem.getID()));
				UpdatePurchaseOrderFragment nextFrag= new UpdatePurchaseOrderFragment();
				nextFrag.setArguments(args);
			     getActivity().getFragmentManager().beginTransaction()
			     .replace(R.id.frame_container, nextFrag)											     
			     .addToBackStack("Update Purchase Order")
			     .commit();							
			}		
			
		});
		
        util= new UtilityFunctions(getActivity());	
        
        
        tblOrders = (TableLayout)rootView.findViewById(R.id.tblPurchaseOrder);
        
        //Build Local Order table
        BuildOrderTable();
    	
		//Create the customer array and adapter
        customerArray = new  ArrayList<AutoCompleteItem>();
        customerArray  = (ArrayList<AutoCompleteItem>) controller.getAllCustomersItems();        
        customerAdapter = new AutocompleteArrayAdapter(getActivity(), R.layout.list_view_row_item, customerArray);
        
        //Creating new order
        btnOrdersNew=(Button)rootView.findViewById(R.id.btnPurchaseOrderNew);
        
        
        	btnOrdersNew.setOnClickListener(new View.OnClickListener() {			
    			@Override
    			public void onClick(View v) {
    				// TODO Auto-generated method stub
    				LayoutInflater layoutInflater = LayoutInflater.from(v.getContext());
    				View promptView = layoutInflater.inflate(R.layout.select_supplier_popup, null);
    				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getContext());
    				alertDialogBuilder.setView(promptView);
    				
    				final Spinner spinSupplier = (Spinner)promptView.findViewById(R.id.spinnerSupplier);
    				
    			    final ArrayAdapter<String> supplierSpinnerAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, names);    			    
    			    supplierSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    			    spinSupplier.setAdapter(supplierSpinnerAdapter);
    			    spinSupplier.setOnItemSelectedListener(new OnItemSelectedListener(){
						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub	
							//ACSelectCustomer.setText(names.get(position));							
							selectedCustomerID= Integer.parseInt(sIds.get(position));
							selectedCustomerName= names.get(position);
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
							// TODO Auto-generated method stub
							
						}
    			    	
    			    });
    			        			        			    
    			    
    				    				
    				alertDialogBuilder
    						.setCancelable(false)
    						.setPositiveButton(getResources().getString(R.string.create_order), new DialogInterface.OnClickListener() {
    									public void onClick(DialogInterface dialog, int id) {		
    										//selectedCustomerID= Integer.parseInt(sIds.get(spinCustomer.getSelectedItemPosition()));
    										
    										if(selectedCustomerID==0 && spinSupplier.getSelectedItemPosition()==-1){
    											Toast.makeText(getActivity(), "Please select the supplier", 10).show();
    											return;
    										}
    										
    										if(selectedCustomerID==0){
    											selectedCustomerID = Integer.parseInt(sIds.get(spinSupplier.getSelectedItemPosition()));
    											selectedCustomerName = names.get(spinSupplier.getSelectedItemPosition());
    										}
    										controller.insertUpdateConfig("last_cust1", selectedCustomerName);
    										Order newOrder = new Order(getActivity());
											newOrder.setCustomerID(String.valueOf(selectedCustomerID));
											
											String salesman = controller.getConfigValue("salesman");
											if(salesman.contentEquals("")){
												salesman="Offline user";
											}	
											
											newOrder.setSalesman(salesman);
											newOrder.setTypeID(Order.KEY_TYPE_PURCHASE_ORDER);
											String LastOrderNo = controller.getConfigValue("lastpurchaseorderno");
											if(LastOrderNo.isEmpty()){
												controller.insertUpdateConfig("lastpurchaseorderno", "0");
												LastOrderNo="0";
											}
											
											int intLastOrderNo=Integer.parseInt(LastOrderNo);
											intLastOrderNo = intLastOrderNo+1;
											controller.insertUpdateConfig("lastpurchaseorderno", String.valueOf(intLastOrderNo));
											newOrder.setIndexID(String.valueOf(intLastOrderNo));
											int localOrderID=Integer.parseInt(newOrder.save());
											if(localOrderID>0){
												Bundle args = new Bundle();
												args.putString("localOrderID", String.valueOf(localOrderID));												 
												Toast.makeText(getActivity(), getResources().getString(R.string.order_created), 10).show();
												UpdatePurchaseOrderFragment nextFrag= new UpdatePurchaseOrderFragment();
												nextFrag.setArguments(args);
											     getActivity().getFragmentManager().beginTransaction()
											     .replace(R.id.frame_container, nextFrag)											     
											     .addToBackStack("Update Purchase Order")
											     .commit();
											}else{
												Toast.makeText(getActivity(), getResources().getString(R.string.unable_to_create_order), 10).show();												
											}
    									}
    								})
    						.setNegativeButton(getResources().getString(R.string.cancel),
    								new DialogInterface.OnClickListener() {
    									public void onClick(DialogInterface dialog,	int id) {
    										dialog.cancel();
    									}
    								});

    				// create an alert dialog
    				AlertDialog alertD = alertDialogBuilder.create();
    				alertD.show();	
    			}
    		});
        
         
        return rootView;
    }
	
	public void BuildOrderTable() {
		List<Order> orders = controller.getOrders(Order.KEY_TYPE_PURCHASE_ORDER);
		customList.clear();		
			//Updating the orders in the table
			if(orders.size()==0){
				Toast.makeText(getActivity(), this.getResources().getString(R.string.no_orders), 10).show();
				adapter.notifyDataSetChanged();				
				return;
			}
			
			for(Order order : orders){				
				CustomListItem listItem = new CustomListItem();
				listItem.setID(order.getID());
				listItem.setTitle(order.getSupplier().getName());				
				listItem.setDescription("Purchase Order " + order.getIndexID());				
				listItem.setFooterRight(order.getDate());
				customList.add(listItem);	
			}	
			adapter.notifyDataSetChanged();
   }
}